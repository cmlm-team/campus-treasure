package routes

import (
	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	gs "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"net/http"
	"web_app/controller"
	_ "web_app/docs"
	"web_app/logger"
	"web_app/middlewares"
)

func SetUp(mode string) (error, *gin.Engine) {
	//gin的日志模式 release or dev
	if mode == gin.ReleaseMode {
		gin.SetMode(gin.ReleaseMode)
	}
	//validator错误翻译器注册
	if err := controller.InitTrans("en"); err != nil {
		return err, nil
	}
	//创建engine实例,处理器
	r := gin.New()
	r.Use(logger.GinLogger(), logger.GinRecovery(true))
	//注册路由

	v1 := r.Group("/api/v1")

	//注册处理器函数
	v1.POST("/signup", controller.RegisterHandler)
	//登陆处理器函数
	v1.POST("/login", controller.Login)

	//获取帖子列表
	v1.GET("/posts2", controller.GetPostListHandler2)
	v1.GET("/posts", controller.GetPostListHandler)
	v1.GET("/community", controller.CommunityHandler)
	v1.GET("/community/:id", controller.CommunityDetailHandler)
	v1.GET("/post/:id", controller.GetPostDetailHandler)
	v1.GET("/comment/:id", controller.GetCommentHandler)

	//应用JWT中间件
	v1.Use(middlewares.JWTAuthMiddleware())

	{
		//创建帖子
		v1.POST("/post", controller.CreatePostHandler)
		// 投票
		v1.POST("/vote", controller.PostVoteController)
		//发表评论
		v1.POST("/comment/:id", controller.PostACommentHandler)
	}
	pprof.Register(r) // 注册pprof相关路由

	r.GET("/swagger/*any", gs.WrapHandler(swaggerFiles.Handler))

	r.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"msg": "404",
		})
	})

	return nil, r
}
