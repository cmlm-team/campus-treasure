package utils

import (
	"crypto/md5"
	"encoding/hex"
)

// SecretId  加密用的常量
const SecretId = "how do i do"

func Encrypt(oString string) string {
	h := md5.New()
	h.Write([]byte(SecretId))
	return hex.EncodeToString(h.Sum([]byte(oString)))
}
