package mysql

import (
	"database/sql"
	"errors"
	"web_app/models"
	encrypt "web_app/utils"
)

// InsertUser 向数据库插入一条新的用户记录
func InsertUser(user *models.User) (err error) {
	sqlStr := "insert into user(user_id,username,password) values(?,?,?)"
	_, err = db.Exec(sqlStr, user.UserID, user.Username, user.Password)
	return err
}

// CheckUserExit 根据用户名检查用户是否重复
func CheckUserExit(username string) (err error) {
	sqlStr := "select count(user_id) from user where username = ?"
	var count int
	if err := db.Get(&count, sqlStr, username); err != nil {
		return err
	}
	if count > 0 {
		return errors.New("用户已存在")
	}
	return
}

// Login 登陆
func Login(user *models.User) (err error) {
	oPassword := user.Password
	sqlStr := "select user_id,username,password from user where username = ?"
	err = db.Get(user, sqlStr, user.Username)
	if err == sql.ErrNoRows {
		return errors.New("用户不存在")
	}
	if err != nil {
		return err
	}
	oPassword = encrypt.Encrypt(oPassword)
	if oPassword != user.Password {
		return errors.New("密码错误")
	}
	return
}

// GetUserById 根据id获取用户信息
func GetUserById(uid int64) (user *models.User, err error) {
	user = new(models.User)
	sqlStr := `select user_id, username from user where user_id = ?`
	err = db.Get(user, sqlStr, uid)
	return
}
