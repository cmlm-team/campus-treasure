package mysql

import "web_app/models"

func CreateComment(p *models.Comment) (err error) {
	sqlStr := `insert into comment(
	comment_id, content, author_id, post_id)
	values (?, ?, ?, ?)
	`
	_, err = db.Exec(sqlStr, p.ID, p.Content, p.AuthorID, p.PostID)
	return
}

func GetComments(pid int64) (comments []*models.Comment, err error) {
	sqlStr := `select 
	author_id, content, create_time
	from comment
	where post_id = ?
	ORDER BY create_time
	DESC 
	`
	comments = make([]*models.Comment, 0, 2)
	err = db.Select(&comments, sqlStr, pid)
	return
}
