package models

import "time"

type Comment struct {
	ID         int64     `json:"id,string" db:"comment_id"`                   // 评论id
	AuthorID   int64     `json:"author_id" db:"author_id" binding:"required"` // 作者id
	PostID     int64     `json:"post_id" db:"post_id" `                       // 帖子id
	Status     int32     `json:"status" db:"status"`                          // 评论状态
	Content    string    `json:"content" db:"content" binding:"required"`     // 评论内容
	CreateTime time.Time `json:"create_time" db:"create_time"`                // 评论创建时间
}
