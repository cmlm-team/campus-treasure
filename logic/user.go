package logic

import (
	"web_app/dao/mysql"
	"web_app/models"
	"web_app/pkg/jwt"
	"web_app/pkg/snowflake"
	encrypt "web_app/utils"
)

// Register 注册业务
func Register(p *models.ParamRegister) (err error) {
	//1.判断用户是否存在
	if err = mysql.CheckUserExit(p.Username); err != nil {
		return err
	}
	//2.生成uid
	userID := snowflake.GenID()
	user := &models.User{
		UserID:   userID,
		Username: p.Username,
		Password: p.Password,
	}
	//3.密码加密
	user.Password = encrypt.Encrypt(user.Password)
	//4.存入数据库,返回错误
	return mysql.InsertUser(user)
}

// Login 登录业务
func Login(p *models.ParamLogin) (user *models.User, err error) {
	user = &models.User{
		Username: p.Username,
		Password: p.Password,
	}
	//登录失败
	//返回错误
	if err = mysql.Login(user); err != nil {
		return nil, err
	}
	//登录成功
	// 生成JWT
	token, err := jwt.GenToken(user.UserID, user.Username)
	if err != nil {
		return
	}
	user.Token = token
	return
}
