package logic

import (
	"go.uber.org/zap"
	"web_app/dao/mysql"
	"web_app/models"
	"web_app/pkg/snowflake"
)

func CreateComment(p *models.Comment) (err error) {
	// 1. 生成comment id
	p.ID = snowflake.GenID()
	// 2. 保存到数据库
	err = mysql.CreateComment(p)
	if err != nil {
		return err
	}
	return
	// 3. 返回
}

func GetCommentById(pid int64) (data []*models.ApiComments, err error) {
	//获取该帖子的全部评论
	comments, err := mysql.GetComments(pid)
	if err != nil {
		return nil, err
	}
	data = make([]*models.ApiComments, 0, len(comments))
	for _, comment := range comments {
		// 根据作者id查询作者信息
		user, err := mysql.GetUserById(comment.AuthorID)
		if err != nil {
			zap.L().Error("mysql.GetUserById(post.AuthorID) failed",
				zap.Int64("author_id", comment.AuthorID),
				zap.Error(err))
			continue
		}

		commentDetail := &models.ApiComments{
			AuthorName: user.Username,
			Comment:    comment,
		}
		data = append(data, commentDetail)
	}
	return
}
