package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"go.uber.org/zap"
	"web_app/logic"
	"web_app/models"
)

// RegisterHandler 用户注册接口
// @Summary 用户注册接口
// @Description 用户注册接口
// @Tags 用户相关接口
// @Accept application/json
// @Produce application/json
// @Param object query models.ParamRegister false "查询参数"
// @Router /api/v1/signup [post]
func RegisterHandler(c *gin.Context) {
	//1.参数校验
	p := new(models.ParamRegister)
	if err := c.ShouldBindJSON(p); err != nil {
		zap.L().Error("Register with invalid param", zap.Error(err))
		//判断err是否validation类型
		errs, ok := err.(validator.ValidationErrors)
		if !ok {
			ResponseError(c, CodeInvalidParam)

		} else {
			ResponseErrorWithMsg(c, CodeInvalidParam, errs.Translate(trans))
		}
		return
	}
	//2.业务处理
	err := logic.Register(p)
	if err != nil {
		zap.L().Error("register failed:", zap.Error(err))
		ResponseErrorWithMsg(c, CodeRegisterFailed, err.Error())
		return
	}
	//3.返回响应
	ResponseSuccess(c, 1000)
}

// Login 用户登陆接口
// @Summary 用户登陆接口
// @Description 用户登陆接口
// @Tags 用户相关接口
// @Accept application/json
// @Produce application/json
// @Param object query models.ParamLogin false "查询参数"
// @Router /api/v1/login [post]
func Login(c *gin.Context) {
	//1.参数校验
	p := new(models.ParamLogin)
	if err := c.ShouldBindJSON(p); err != nil {
		zap.L().Error("Login with invalid param", zap.Error(err))
		errs, ok := err.(validator.ValidationErrors)
		if !ok {
			ResponseError(c, CodeInvalidParam)
		} else {
			ResponseErrorWithMsg(c, CodeInvalidParam, errs.Translate(trans))
		}
		return
	}
	//2.业务处理
	user, err := logic.Login(p)
	if err != nil {
		zap.L().Error("login failed:", zap.Error(err))
		ResponseErrorWithMsg(c, CodeLoginFailed, err.Error())
		return
	}
	//3.返回响应
	ResponseSuccess(c, gin.H{
		"user_id":   fmt.Sprintf("%d", user.UserID),
		"user_name": user.Username,
		"token":     user.Token,
	})
}
