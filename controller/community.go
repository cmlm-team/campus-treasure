package controller

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"strconv"
	"web_app/logic"
)

// CommunityHandler 获取所有帖子分学院信息接口
// @Summary 获取所有帖子分学院信息接口
// @Description 获取所有帖子分学院信息接口
// @Tags 分学院相关接口
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Router /api/v1/community [get]
func CommunityHandler(c *gin.Context) {
	//将所有的分区信息拿到手
	data, err := logic.GetCommunityList()
	if err != nil {
		zap.L().Error("logic.GetCommunityList() failed", zap.Error(err))
		ResponseError(c, CodeServerBusy)
		return
	}
	ResponseSuccess(c, data)

}

// CommunityDetailHandler 学院分区分类详情接口
// @Summary 学院分区分类详情接口
// @Description 学院分区分类详情接口
// @Tags 分学院相关接口
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Router /api/v1/community/:id [get]
func CommunityDetailHandler(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		ResponseError(c, CodeInvalidParam)
		return
	}
	data, err := logic.GetCommunityDetail(id)
	if err != nil {
		zap.L().Error("logic.GetCommunityDetail() failed", zap.Error(err))
		ResponseError(c, CodeServerBusy)
	}
	ResponseSuccess(c, data)
}
