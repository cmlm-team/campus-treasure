package controller

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"strconv"
	"web_app/logic"
	"web_app/models"
)

// PostACommentHandler 发送评论接口
// @Summary 发送评论接口
// @Description 按照帖子id给comment数据库添加记录
// @Tags 评论相关接口
// @Accept application/json
// @Produce application/json
// @Param Authorization header string false "Bearer 用户令牌"
// @Security ApiKeyAuth
// @Param object query models.Comment false "查询参数"
// @Router /api/v1/comment/:id [post]
func PostACommentHandler(c *gin.Context) {
	//参数校验
	p := new(models.Comment)
	if err := c.ShouldBindJSON(p); err != nil {
		zap.L().Debug("c.ShouldBindJSON(p) error", zap.Any("err", err))
		zap.L().Error("create comment with invalid param")
		ResponseError(c, CodeInvalidParam)
		return
	}
	// 从 c 取到当前发请求的用户的ID
	userID, err := getCurrentUserID(c)
	if err != nil {
		ResponseError(c, CodeNeedLogin)
		return
	}
	p.AuthorID = userID

	// 从 c 取到当前发请求的帖子的ID
	pidStr := c.Param("id")
	pid, err := strconv.ParseInt(pidStr, 10, 64)
	if err != nil {
		ResponseError(c, CodeNeedLogin)
		return
	}
	p.PostID = pid
	if err = logic.CreateComment(p); err != nil {
		zap.L().Error("logic.CreateComment(p) failed", zap.Error(err))
		ResponseError(c, CodeServerBusy)
		return
	}
	ResponseSuccess(c, CodeSuccess)

}

// GetCommentHandler 获取帖子全部评论接口
// @Summary 获取帖子全部评论接口
// @Description 按时间排序查询评论接口
// @Tags 评论相关接口
// @Accept application/json
// @Produce application/json
// @Router /api/v1/comment/:id [get]
func GetCommentHandler(c *gin.Context) {
	pidStr := c.Param("id")
	pid, err := strconv.ParseInt(pidStr, 10, 64)
	if err != nil {
		zap.L().Error("get comment detail with invalid param", zap.Error(err))
		ResponseError(c, CodeInvalidParam)
		return
	}
	// 2. 根据id取出帖子数据（查数据库）
	data, err := logic.GetCommentById(pid)
	if err != nil {
		zap.L().Error("logic.GetCommentsById(pid) failed", zap.Error(err))
		ResponseError(c, CodeServerBusy)
		return
	}
	// 3. 返回响应
	ResponseSuccess(c, data)
}
